#include <iostream>
using namespace std;

int* GetArrayDangerous()
{
	int myArray[100];
	for (int i = 0; i < 100; ++i)
	{
		myArray[i];
	}
	return myArray;  // No, don't return stack-allocated memory addresses!
}

void PrintReverse(int* numbers, int size)
{
	for (int i = size-1; i >= 0; --i)
	{
		cout << "Element: " << numbers[i] << endl;
	}
}

void PrintReverse2(int* numbers, int size)
{
	for (int* p = numbers + (size - 1); p != numbers; --p)
	{
		cout << "Element: " << *p << endl;
	}
	cout << "Element: " << *numbers << endl;
}

void PrintReverseRecursive(int* numbers, int size)
{
	// Termination/base case
	if (size == 0)
		return;

	cout << numbers[size-1] << endl;
	// Change in argument
	PrintReverseRecursive(numbers, size - 1);  // Recursive call
}

int main(int argc, char* argv[])
{
	// Let's leak a lot!
	/*for (int i = 0; 1; ++i)
	{
		int* myPtr = new int();
	}*/

	// Don't.
	//int* myArray = GetArrayDangerous();
	// Any more stack-allocated variables may overwrite your "array"

	int numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};

	PrintReverse(numbers, 9);
	cout << "..." << endl;
	PrintReverse2(numbers, 9);
	cout << "..." << endl;
	PrintReverseRecursive(numbers, 9);


	cout << "Press enter..." << endl;
	cin.get();

	return 0;
}