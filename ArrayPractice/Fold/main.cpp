#include <iostream>
#include <string>
using namespace std;

struct Pair
{
public:
	string first;
	int second;
};

//void Fold(Pair pairs[9], string names[9], int favoriteNumbers[9])
//void Fold(Pair* pairs, string* names, int* favoriteNumbers)
/*void Fold(Pair pairs[], string names[], int favoriteNumbers[])
{
	for (int i = 0; i < 9; ++i)
	{
		pairs[i].first = names[i];
		pairs[i].second = favoriteNumbers[i];
	}
}*/

void Fold(Pair* pairs, string* names, int* favoriteNumbers, int size)
{
	if (size == 0)
		return;

	{
		pairs[0].first = names[0];
		pairs[0].second = favoriteNumbers[0];
	}

	Fold(pairs + 1, names + 1, favoriteNumbers + 1, size-1);
}

/*int Find(int targetValue, int* values, int size)
{
	for (int i = 0; i < size; ++i)
	{
		if (values[i] == targetValue)
			return i;
	}
	return -1;
}*/

int Find(int targetValue, int* values, int size)
{
	if (size == 0)
		return -1;
	if (values[size-1] == targetValue)
		return size-1;
	return Find(targetValue, values, size - 1);
}

int main(int argc, char* argv[])
{
	string names[] = {"Jon", "Connor", "Andy", "Wentao", "Phantasma", "Dana", "Josh", "Binh", "Patrick"};
	int favoriteNumbers[] = {4, 138, 7, 5, 5, 12, 8, 6, 10};
	const int size = 9;

	Pair* pairs = new Pair[size];
	// Define a function that fills the pairs array with 
	// a Pair for each matching name and favortie number.
	Fold(pairs, names, favoriteNumbers, size);

	for (int i = 0; i < size; ++i)
	{
		cout << "Pair: " << pairs[i].first << ", " << pairs[i].second << endl;
	}

	delete[] pairs;


	// Homework 2: Implement using a loop, then with recursion.
	// Returns the index where 7 is found.  If it's not found, return -1.
	int index = Find(7, favoriteNumbers, size);
	cout << "Found 7 at index " << index << ", expected 2." << endl;
	if (index != 2)
	{
		cin.get();
		return 23546;
	}

	cin.get();
	return 0;
}