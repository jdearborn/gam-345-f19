#include <iostream>
using namespace std;

int main(int argc, char* argv[])
{
	cout << "Hello World!" << endl;

	// new
	// []
	// int

	// C++09 -> 09++ -> 10
	// C++09++ -> C++0x -> C++11

	// From 30 -> 60 in multiples of 3
	int* myNumbers = new int[11];
	for (int i = 0; i < 11; ++i)
	{
		myNumbers[i] = i * 3 + 30;
	}
	/*for (int i = 30; i < 61; i += 3)
	{
		myNumbers[(i - 30) / 3] = i;
	}*/
	/*for (int i = 0, int v = 30; i < 11; ++i, v += 3)
	{
		myNumbers[i] = v;
	}*/
	/*int* ptr = myNumbers;
	for (int v = 30; v < 61; v += 3, ++ptr)
	{
		*ptr = v;
	}*/


	return 0;
}