#include <iostream>
using namespace std;


// Search this vector, return the index where we find this value.
int BinarySearch(int* numbers, int size, int value)
{
	// Compare to the middle element
	// If the target is lesser, recursively search the left subarry
	// If the target is greater, recursively search the right subarray

	if (size == 0)
		return -1;

	if (numbers[size / 2] > value)
	{
		// Search left side
		return BinarySearch(numbers, size/2, value);
	}
	else if(numbers[size/2] < value)
	{
		// Search right side
		return (size + 1) / 2 + BinarySearch(numbers + (size+1)/2, (size+1) / 2, value);
	}
	else
	{
		return size/2;
	}
}


int main(int argc, char argv[])
{
	int numbers[] = { 4, 6, 9, 10, 12, 14, 20, 37 };
	int size = 8;


	int index = BinarySearch(numbers, size, 20);


	return 0;
}