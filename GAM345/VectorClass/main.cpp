#include "Vector.h"
#include <iostream>
using namespace std;


int TestConstructor()
{
	try {
		Vector<int> v;
		return 1;
	}
	catch (...)
	{
		cout << "Exception in constructor" << endl;
		return 0;
	}
}

int TestSize()
{
	bool failed = false;
	try
	{
		Vector<int> v;
		if (v.Size() != 0)
		{
			cout << "Bad size 0" << endl;
			failed = true;
		}
		return !failed;
	}
	catch (...)
	{
		cout << "Exception in TestSize0()" << endl;
		return 0;
	}
}

int TestPushBack()
{
	bool failed = false;
	try
	{
		Vector<int> v;
		v.PushBack(4);
		if (v.Size() != 1)
		{
			cout << "Bad size 1" << endl;
			failed = true;
		}
		v.PushBack(5);
		if (v.Size() != 2)
		{
			cout << "Bad size 2" << endl;
			failed = true;
		}
		v.PushBack(6);
		if (v.Size() != 3)
		{
			cout << "Bad size 3" << endl;
			failed = true;
		}
		return !failed;
	}
	catch (...)
	{
		cout << "Exception in TestPushBack()" << endl;
		return 0;
	}
}

int TestAt()
{
	bool failed = false;
	try
	{
		Vector<int> v;
		v.PushBack(4);
		if (v.At(0) != 4)
		{
			cout << "Bad At 1" << endl;
			failed = true;
		}
		v.PushBack(5);
		if (v.At(0) != 4 || v.At(1) != 5)
		{
			cout << "Bad At 2" << endl;
			failed = true;
		}
		v.PushBack(6);
		if (v[2] != 6)
		{
			cout << "Bad At 3" << endl;
			failed = true;
		}

		try
		{
			v.At(-50);
			cout << "Bad At: Failed to raise exception for negative index." << endl;
			failed = true;
		}
		catch (...)
		{
			// We good?  Maybe.
		}
		try
		{
			v.At(50);
			cout << "Bad At: Failed to raise exception for out-of-bounds." << endl;
			failed = true;
		}
		catch (...)
		{
			// We good?  Maybe.
		}

		return !failed;
	}
	catch (...)
	{
		cout << "Exception in TestAt()" << endl;
		return 0;
	}
}

int TestCopy()
{
	bool failed = false;
	try
	{
		Vector<int> v1;
		Vector<int> v2(v1);
		if (v2.Size() != 0)
		{
			cout << "Bad copy constructor 0" << endl;
			failed = true;
		}

		Vector<int> v3;
		for (int i = 0; i < 100; ++i)
		{
			v3.PushBack(i);
		}

		Vector<int> v4(v3);
		if (v4.Size() != 100)
		{
			cout << "Bad copy constructor 100" << endl;
			failed = true;
		}

		v4 = v1;
		if (v4.Size() != 0)
		{
			cout << "Bad copy assignment 0" << endl;
			failed = true;
		}

		v4 = v3;
		if (v4.Size() != 100)
		{
			cout << "Bad copy assignment 100" << endl;
			failed = true;
		}
		for (int i = 0; i < v4.Size(); ++i)
		{
			if (v4[i] != i)
			{
				cout << "Bad copy values check" << endl;
				failed = true;
				break;
			}
		}
		return !failed;
	}
	catch (...)
	{
		cout << "Exception in TestCopy()" << endl;
		return 0;
	}
}

int TestPushFront()
{
	bool failed = false;
	try
	{
		Vector<int> v;
		v.PushFront(2);
		if (v.Size() != 1)
		{
			cout << "Bad size front 1" << endl;
			failed = true;
		}
		v.PushFront(1);
		if (v.Size() != 2)
		{
			cout << "Bad size front 2" << endl;
			failed = true;
		}
		v.PushFront(0);
		if (v.Size() != 3)
		{
			cout << "Bad size front 3" << endl;
			failed = true;
		}
		for (int i = 0; i < v.Size(); ++i)
		{
			if (v[i] != i)
			{
				cout << "Bad push front values check" << endl;
				failed = true;
				break;
			}
		}
		return !failed;
	}
	catch (...)
	{
		cout << "Exception in TestPushFront()" << endl;
		return 0;
	}
}

int TestClear()
{
	bool failed = false;
	try
	{
		Vector<int> v1;
		v1.Clear();
		if (v1.Size() != 0)
		{
			cout << "Bad clear 0" << endl;
			failed = true;
		}

		for (int i = 0; i < 100; ++i)
		{
			v1.PushBack(i);
		}

		v1.Clear();
		if (v1.Size() != 0)
		{
			cout << "Bad clear 100" << endl;
			failed = true;
		}

		for (int i = 0; i < 100; ++i)
		{
			v1.PushBack(i);
		}

		v1.Clear();
		if (v1.Size() != 0)
		{
			cout << "Bad clear 100 again" << endl;
			failed = true;
		}

		return !failed;
	}
	catch (...)
	{
		cout << "Exception in TestClear()" << endl;
		return 0;
	}
}

int TestEraseAt()
{
	bool failed = false;
	try
	{
		Vector<int> v;
		for (int i = 0; i < 100; ++i)
		{
			v.PushBack(i);
		}

		v.EraseAt(0);

		for (int i = 0; i < v.Size(); ++i)
		{
			if (v[i] != i + 1)
			{
				cout << "Bad EraseAt 0 values check" << endl;
				failed = true;
				break;
			}
		}

		Vector<int> v2;
		for (int i = 0; i < 100; ++i)
		{
			v2.PushBack(i);
		}

		v2.EraseAt(50);

		for (int i = 0; i < v2.Size(); ++i)
		{
			if ((i < 50 && v2[i] != i) || (i >= 50 && v2[i] != i + 1))
			{
				cout << "Bad EraseAt 50 values check" << endl;
				failed = true;
				break;
			}
		}

		return !failed;
	}
	catch (...)
	{
		cout << "Exception in TestEraseAt()" << endl;
		return 0;
	}
}

int TestErase()
{
	bool failed = false;
	try
	{
		Vector<int> v;
		for (int i = 0; i < 100; ++i)
		{
			v.PushBack(i);
		}

		v.Erase(0);

		if (v.Size() != 99)
		{
			cout << "Bad size erase 0" << endl;
			failed = true;
		}

		for (int i = 0; i < v.Size(); ++i)
		{
			if (v[i] != i + 1)
			{
				cout << "Bad Erase 0 values check" << endl;
				failed = true;
				break;
			}
		}

		Vector<int> v2;
		v2.PushBack(0);
		v2.PushBack(1);
		v2.PushBack(2);
		v2.PushBack(3);
		v2.PushBack(3);
		v2.PushBack(4);
		v2.PushBack(5);

		v2.Erase(3);



		if (v2.Size() != 6)
		{
			cout << "Bad size erase 3" << endl;
			failed = true;
		}

		for (int i = 0; i < v2.Size(); ++i)
		{
			if (v2[i] != i)
			{
				cout << "Bad Erase 3 values check" << endl;
				failed = true;
				break;
			}
		}

		return !failed;
	}
	catch (...)
	{
		cout << "Exception in TestErase()" << endl;
		return 0;
	}
}

int TestFind()
{
	bool failed = false;
	try
	{
		Vector<int> v;
		for (int i = 100; i >= 0; --i)
		{
			v.PushBack(i);
		}

		int n = v.Find(100);

		if (n != 0)
		{
			cout << "Bad find 100" << endl;
			failed = true;
		}


		Vector<int> v2;
		v2.PushBack(3);
		v2.PushBack(3);
		v2.PushBack(3);
		v2.PushBack(3);
		v2.PushBack(3);
		v2.PushBack(3);

		n = v2.Find(3);

		if (n != 0)
		{
			cout << "Bad find 3" << endl;
			failed = true;
		}


		Vector<int> v3;
		v3.PushBack(1);
		v3.PushBack(1);
		v3.PushBack(1);
		v3.PushBack(4);
		v3.PushBack(1);
		v3.PushBack(1);

		n = v3.Find(4);

		if (n != 3)
		{
			cout << "Bad find 4" << endl;
			failed = true;
		}


		n = v3.Find(8);

		if (n != -1)
		{
			cout << "Bad find fail" << endl;
			failed = true;
		}

		return !failed;
	}
	catch (...)
	{
		cout << "Exception in TestFind()" << endl;
		return 0;
	}
}

int TestContains()
{
	bool failed = false;
	try
	{
		Vector<int> v;
		for (int i = 100; i >= 0; --i)
		{
			v.PushBack(i);
		}

		if (!v.Contains(50))
		{
			cout << "Bad contains 50" << endl;
			failed = true;
		}


		Vector<int> v2;
		v2.PushBack(0);
		v2.PushBack(1);
		v2.PushBack(2);
		v2.PushBack(3);
		v2.PushBack(4);
		v2.PushBack(5);

		if (v2.Contains(27))
		{
			cout << "Bad contains fail 27" << endl;
			failed = true;
		}

		return !failed;
	}
	catch (...)
	{
		cout << "Exception in TestContains()" << endl;
		return 0;
	}
}

int TestInsert()
{
	bool failed = false;
	try
	{
		try
		{
			Vector<int> v;
			for (int i = 100; i >= 0; --i)
			{
				v.Insert(i, 0);
			}

			for (int i = 0; i < v.Size(); ++i)
			{
				if (v[i] != i)
				{
					cout << "Bad Insert 0 values check" << endl;
					failed = true;
					break;
				}
			}
		}
		catch (...)
		{
			cout << "Exception Insert 0" << endl;
			failed = true;
		}

		Vector<int> v2;
		for (int i = 0; i < 100; ++i)
		{
			if (i != 50)
				v2.PushBack(i);
		}

		v2.Insert(50, 50);

		for (int i = 0; i < v2.Size(); ++i)
		{
			if (v2[i] != i)
			{
				cout << "Bad Insert 50 values check" << endl;
				failed = true;
				break;
			}
		}

		return !failed;
	}
	catch (...)
	{
		cout << "Exception in TestInsert()" << endl;
		return 0;
	}
}

int TestResize()
{
	bool failed = false;
	try
	{
		Vector<int> v1;
		v1.Resize(0);
		if (v1.Size() != 0)
		{
			cout << "Bad resize 0" << endl;
			failed = true;
		}

		for (int i = 0; i < 100; ++i)
		{
			v1.PushBack(i);
		}
		v1.Resize(50);

		if (v1.Size() != 50)
		{
			cout << "Bad resize 50" << endl;
			failed = true;
		}

		v1.Resize(70);

		if (v1.Size() != 70)
		{
			cout << "Bad resize 70" << endl;
			failed = true;
		}

		return !failed;
	}
	catch (...)
	{
		cout << "Exception in TestResize()" << endl;
		return 0;
	}
}

int main(int argc, char* argv[])
{
	int goodCount = 0;
	int testCount = 0;

	goodCount += TestConstructor();
	testCount++;

	goodCount += TestSize();
	testCount++;

	goodCount += TestPushBack();
	testCount++;

	goodCount += TestAt();
	testCount++;

	goodCount += TestCopy();
	testCount++;

	goodCount += TestPushFront();
	testCount++;

	goodCount += TestClear();
	testCount++;

	goodCount += TestEraseAt();
	testCount++;

	goodCount += TestErase();
	testCount++;

	goodCount += TestFind();
	testCount++;

	goodCount += TestContains();
	testCount++;

	goodCount += TestInsert();
	testCount++;

	goodCount += TestResize();
	testCount++;


	cout << endl << "Result: " << (goodCount/(float)testCount)*100 << "% tests passing" << endl;
	cin.get();

	return 0;
}