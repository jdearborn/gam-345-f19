

#include "Vector.h"

#ifndef __VECTOR_INL__
#define __VECTOR_INL__
#include <cstring>
#include <stdexcept>

template<typename T>
Vector<T>::Vector()
	: capacity(50), contents(new T[capacity]), size(0)
{
	/*size = 0;
	capacity = 50;
	contents = new int[capacity];*/
}

template<typename T>
Vector<T>::Vector(const Vector<T>& other)
{
	contents = nullptr;

	*this = other;
}


template<typename T>
Vector<T>::~Vector()
{
	delete[] contents;
}

template<typename T>
Vector<T>& Vector<T>::operator=(const Vector<T>& other)
{
	delete[] contents;

	size = other.size;
	capacity = other.capacity;
	contents = new T[capacity];
	memcpy(contents, other.contents, size * sizeof(T));

	return *this;
}

template<typename T>
void Vector<T>::PushBack(const T& value)
{
	if (size + 1 > capacity)
	{
		Reserve(size * 2 + 1);  // Adding one is not good!  Let's use a "heuristic".
	}
	contents[size] = value;
	size++;
}

template<typename T>
void Vector<T>::PushFront(const T& value)
{
	if (size + 1 > capacity)
	{
		Reserve(size * 2 + 1);
	}
	for (int i = size; i > 0; --i)
	{
		contents[i] = contents[i-1];
	}
	contents[0] = value;
	size++;
}

template<typename T>
void Vector<T>::Insert(const T& value, int index)
{
	if (size + 1 > capacity)
	{
		Reserve(size * 2 + 1);
	}
	for (int i = size; i > index; --i)
	{
		contents[i] = contents[i - 1];
	}
	contents[index] = value;
	size++;
}

template<typename T>
void Vector<T>::Reserve(int newCapacity)
{
	if (capacity < newCapacity)
	{
		T* newContents = new T[newCapacity];

		/*for (int i = 0; i < size; ++i)
		{
			newContents[i] = contents[i];
		}*/
		memcpy(newContents, contents, size * sizeof(T));

		delete[] contents;
		contents = newContents;
		capacity = newCapacity;
	}
}


template<typename T>
void Vector<T>::Resize(int newSize)
{
	if (newSize < 0)
		return;

	T* newContents = new T[newSize];

	memcpy(newContents, contents, newSize * sizeof(T));

	delete[] contents;
	contents = newContents;
	capacity = newSize;
	size = newSize;
}

template<typename T>
T& Vector<T>::At(int index)
{
	if (index < 0 || index >= size)
		throw std::runtime_error("At(): Invalid index");
	return contents[index];
}

template<typename T>
T& Vector<T>::operator[](int index)
{
	return At(index);
}

template<typename T>
int Vector<T>::Size() const
{
	return size;
}

template<typename T>
void Vector<T>::Clear()
{
	delete[] contents;

	size = 0;
	capacity = 50;
	contents = new int[capacity];
}

template<typename T>
void Vector<T>::EraseAt(int index)
{

}

template<typename T>
void Vector<T>::Erase(const T& value)
{

}

template<typename T>
int Vector<T>::Find(const T& value)
{
	return 0;
}

template<typename T>
bool Vector<T>::Contains(const T& value)
{
	return Find(value) != -1;
}

#endif