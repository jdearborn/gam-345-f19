#include <iostream>
#include <fstream>
#include <chrono>
#include "../VectorClass/Vector.h"
#include "../LinkedList class/LinkedList.h"
using namespace std;
using namespace std::chrono;




void PerfTestVectorPushBack(int minN, int step, int maxN)
{

	ofstream fout;
	fout.open("Vector PushBack.txt");
	fout << "# Vector<int>::PushBack()" << endl;
	fout << "# N\tNanoseconds" << endl;

	for (int n = minN; n < maxN; n += step)
	{
		// Set up vector
		Vector<int> container;
		for (int i = 0; i < n; ++i)
		{
			container.PushBack(i);
		}

		high_resolution_clock::time_point start = high_resolution_clock::now();

		container.PushBack(127);

		high_resolution_clock::time_point finish = high_resolution_clock::now();
		nanoseconds totalNS = duration_cast<nanoseconds>(finish - start);

		fout << n << "\t" << totalNS.count() << endl;
	}

	fout.close();

}

void PerfTestVectorPushFront(int minN, int step, int maxN)
{

	ofstream fout;
	fout.open("Vector PushFront.txt");
	fout << "# Vector<int>::PushFront()" << endl;
	fout << "# N\tNanoseconds" << endl;

	for (int n = minN; n < maxN; n += step)
	{
		// Set up vector
		Vector<int> container;
		for (int i = 0; i < n; ++i)
		{
			container.PushBack(i);
		}

		high_resolution_clock::time_point start = high_resolution_clock::now();

		container.PushFront(127);

		high_resolution_clock::time_point finish = high_resolution_clock::now();
		nanoseconds totalNS = duration_cast<nanoseconds>(finish - start);

		fout << n << "\t" << totalNS.count() << endl;
	}

	fout.close();

}

void PerfTestVectorInsert(int minN, int step, int maxN)
{

	ofstream fout;
	fout.open("Vector Insert.txt");
	fout << "# Vector<int>::Insert()" << endl;
	fout << "# N\tNanoseconds" << endl;

	for (int n = minN; n < maxN; n += step)
	{
		// Set up vector
		Vector<int> container;
		for (int i = 0; i < n; ++i)
		{
			container.PushBack(i);
		}

		high_resolution_clock::time_point start = high_resolution_clock::now();

		container.Insert(127, n/2);

		high_resolution_clock::time_point finish = high_resolution_clock::now();
		nanoseconds totalNS = duration_cast<nanoseconds>(finish - start);

		fout << n << "\t" << totalNS.count() << endl;
	}

	fout.close();

}






void PerfTestLinkedListPushBack(int minN, int step, int maxN)
{

	ofstream fout;
	fout.open("LinkedList PushBack.txt");
	fout << "# LinkedList<int>::PushBack()" << endl;
	fout << "# N\tNanoseconds" << endl;

	for (int n = minN; n < maxN; n += step)
	{
		// Set up vector
		LinkedList<int> container;
		for (int i = 0; i < n; ++i)
		{
			container.PushFront(i);
		}

		high_resolution_clock::time_point start = high_resolution_clock::now();

		container.PushBack(127);

		high_resolution_clock::time_point finish = high_resolution_clock::now();
		nanoseconds totalNS = duration_cast<nanoseconds>(finish - start);

		fout << n << "\t" << totalNS.count() << endl;
	}

	fout.close();

}

void PerfTestLinkedListPushFront(int minN, int step, int maxN)
{

	ofstream fout;
	fout.open("LinkedList PushFront.txt");
	fout << "# LinkedList<int>::PushFront()" << endl;
	fout << "# N\tNanoseconds" << endl;

	for (int n = minN; n < maxN; n += step)
	{
		// Set up vector
		LinkedList<int> container;
		for (int i = 0; i < n; ++i)
		{
			container.PushFront(i);
		}

		high_resolution_clock::time_point start = high_resolution_clock::now();

		container.PushFront(127);

		high_resolution_clock::time_point finish = high_resolution_clock::now();
		nanoseconds totalNS = duration_cast<nanoseconds>(finish - start);

		fout << n << "\t" << totalNS.count() << endl;
	}

	fout.close();

}

void PerfTestLinkedListInsert(int minN, int step, int maxN)
{

	ofstream fout;
	fout.open("LinkedList Insert.txt");
	fout << "# LinkedList<int>::Insert()" << endl;
	fout << "# N\tNanoseconds" << endl;

	for (int n = minN; n < maxN; n += step)
	{
		// Set up LinkedList
		LinkedList<int> container;
		for (int i = 0; i < n; ++i)
		{
			container.PushFront(i);
		}

		high_resolution_clock::time_point start = high_resolution_clock::now();

		container.Insert(127, n / 2);

		high_resolution_clock::time_point finish = high_resolution_clock::now();
		nanoseconds totalNS = duration_cast<nanoseconds>(finish - start);

		fout << n << "\t" << totalNS.count() << endl;
	}

	fout.close();

}


int main(int argc, char* argv[])
{
	PerfTestVectorPushBack(1, 1000, 100000);
	PerfTestVectorPushFront(1, 1000, 100000);
	PerfTestVectorInsert(1, 1000, 100000);

	PerfTestLinkedListPushBack(1, 1000, 100000);
	PerfTestLinkedListPushFront(1, 1000, 100000);
	PerfTestLinkedListInsert(1, 1000, 100000);

	system("gnuplot \"plot-Vector and List.txt\"");

	return 0;
}