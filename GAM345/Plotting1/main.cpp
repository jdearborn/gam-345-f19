#include <iostream>
#include "Vector.h"
#include <fstream>
using namespace std;

int main(int argc, char* argv[])
{
	// Output formats
	// csv (Comma-Separated Values)

	// gnuplot format (columns of data - tab-separated, '\t' )


	ofstream fout;
	fout.open("MyData.txt");

	fout << "# Vector PushBack" << endl;
	fout << "# N\tTime (ns)" << endl;

	int n = 1;
	double time = 57.0;

	fout << n << '\t' << time << endl;
	fout << n << '\t' << time << endl;
	fout << n << '\t' << time << endl;
	fout << n << '\t' << time << endl;
	fout << n << '\t' << time << endl;
	fout << n << '\t' << time << endl;


	fout.close();

	system("gnuplot my_plot_script.txt");


	return 0;
}