#pragma once


template<typename T>
struct Node
{
public:
	T value;
	Node* next;

	Node()
		: value(), next(nullptr)
	{}

	Node(const T& value)
		: value(value), next(nullptr)
	{}
};

template<typename T>
class LinkedList
{
public:
	LinkedList();
	LinkedList(const LinkedList& other);
	~LinkedList();

	LinkedList& operator=(const LinkedList& other);

	void PushBack(const T& value);
	void PushFront(const T& value);
	void Insert(const T& value, int index);
	T& At(int index);
	T& operator[](int index);
	int Size() const;


private:
	Node<T>* head;
	int size;
};

#include "LinkedList.inl"