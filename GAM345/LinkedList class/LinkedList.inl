

#include "LinkedList.h"

#ifndef __LINKEDLIST_INL__
#define __LINKEDLIST_INL__
#include <cstring>
#include <stdexcept>

template<typename T>
LinkedList<T>::LinkedList()
	: head(nullptr), size(0)
{}

template<typename T>
LinkedList<T>::LinkedList(const LinkedList<T>& other)
	: head(nullptr), size(0)
{
	*this = other;
}


template<typename T>
LinkedList<T>::~LinkedList()
{
	Node<T>* n = head;
	while(n != nullptr)
	{
		Node<T>* toDelete = n;
		n = n->next;
		delete toDelete;
	}
}

template<typename T>
LinkedList<T>& LinkedList<T>::operator=(const LinkedList<T>& other)
{
	for (Node<T>* n = head; n != nullptr; n = n->next)
	{
		delete n;
	}

	// Copy each node
	Node<T>* lastNode = head;
	for (Node<T>* n = other.head; n != nullptr; n = n->next)
	{
		Node<T>* newNode = new Node<T>(n->value);
		if (lastNode == nullptr)
		{
			head = newNode;
		}
		else
		{
			lastNode->next = newNode;
		}
		lastNode = newNode;
	}

	size = other.size;

	return *this;
}

template<typename T>
void LinkedList<T>::PushBack(const T& value)
{
	// Get to the end of the list and add a node
	if (head == nullptr)
	{
		head = new Node<T>(value);
	}
	else
	{
		Node<T>* n = head;
		while (n->next != nullptr)
		{
			n = n->next;
		}
		n->next = new Node<T>(value);
	}
	size++;
}

template<typename T>
void LinkedList<T>::PushFront(const T& value)
{
	// Make a new node for the new head
	if (head == nullptr)
	{
		head = new Node<T>(value);
	}
	else
	{
		Node<T>* n = new Node<T>(value);
		n->next = head;
		head = n;
	}
	size++;
}

template<typename T>
void LinkedList<T>::Insert(const T& value, int index)
{
	// Get to the right spot of the list and add a node
	if (head == nullptr)
	{
		head = new Node<T>(value);
	}
	else if (index == 0)
	{
		Node<T>* newNode = new Node<T>(value);
		newNode->next = head;
		head = newNode;
	}
	else
	{
		Node<T>* lastNode = head;
		for (Node<T>* n = lastNode->next; n != nullptr; n = n->next)
		{
			--index;
			if (index == 0)
			{
				Node<T>* newNode = new Node<T>(value);
				newNode->next = lastNode->next;
				lastNode->next = newNode;
				break;
			}
		}
	}
	size++;
}


template<typename T>
T& LinkedList<T>::At(int index)
{
	if (index < 0 || index >= size)
		throw std::runtime_error("At(): Invalid index");
	
	// Linear traversal
	Node<T>* n = head;
	while (index > 0 && n->next != nullptr)  // Stop when we're at the right index or there's nothing after this one
	{
		n = n->next;
		--index;
	}
	return n->value;
}

template<typename T>
T& LinkedList<T>::operator[](int index)
{
	return At(index);
}

template<typename T>
int LinkedList<T>::Size() const
{
	return size;
}

#endif