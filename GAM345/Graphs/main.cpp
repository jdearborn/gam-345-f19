#include <iostream>
#include <string>
#include <map>
#include <list>
#include <queue>
#include <set>
#include <functional>
using namespace std;


class Node
{
public:
	string id;

	Node()
	{}
	Node(const string& id)
		: id(id)
	{}
};

class Graph
{
public:
	Graph()
	{}
	virtual ~Graph()
	{}

	virtual int GetNumNodes() = 0;
	virtual int GetNumEdges() = 0;

	virtual void AddEdge(Node* n1, Node* n2, float weight) = 0;
	virtual void RemoveEdge(Node* n1, Node* n2) = 0;

	virtual bool HasEdge(Node* n1, Node* n2) = 0;
	virtual float GetWeight(Node* n1, Node* n2) = 0;

	virtual Node* GetFirstNeighbor(Node* n) = 0;
	virtual Node* GetNextNeighbor(Node* n, Node* lastNeighbor) = 0;
};

class Edge
{
public:
	Node* node1;
	Node* node2;
	float weight;

	Edge()
		: node1(nullptr), node2(nullptr), weight(1.0f)
	{}

	Edge(Node* n1, Node* n2, float w)
		: node1(n1), node2(n2), weight(w)
	{}
};


class ListGraph : public Graph
{
private:
	map<Node*, list<Edge>> dict;

public:
	ListGraph()
	{}
	virtual ~ListGraph()
	{}

	virtual int GetNumNodes() override
	{
		return dict.size();
	}
	virtual int GetNumEdges() override
	{
		int count = 0;
		for (auto p : dict)
		{
			list<Edge>& l = p.second;
			count += l.size();
		}
		return count;
	}

	virtual void AddEdge(Node* n1, Node* n2, float weight) override
	{
		// Make sure there's an empty list to start from
		dict.insert(make_pair(n1, list<Edge>()));

		dict[n1].push_back(Edge(n1, n2, weight));
	}

	void AddBidirectionalEdge(Node* n1, Node* n2, float weight)
	{
		AddEdge(n1, n2, weight);
		AddEdge(n2, n1, weight);
	}

	virtual void RemoveEdge(Node* n1, Node* n2) override
	{}

	virtual bool HasEdge(Node* n1, Node* n2) override
	{
		return false;
	}
	virtual float GetWeight(Node* n1, Node* n2) override
	{
		return 1.0f;
	}

	virtual Node* GetFirstNeighbor(Node* n) override
	{
		return nullptr;
	}
	virtual Node* GetNextNeighbor(Node* n, Node* lastNeighbor) override
	{
		return nullptr;
	}

	list<Edge> GetEdges(Node* n)
	{
		auto e = dict.find(n);
		if (e == dict.end())
			return list<Edge>();

		return e->second;
	}
};

// Not as much of a "search" as it is an "exploration"
void BreadthFirstSearch(ListGraph& g, Node* start, function<void(Node*)> visitor)
{
	queue<Node*> unvisited;
	set<Node*> visited;

	Node* current = start;

	while(true)
	{
		// Only visit unvisited nodes
		if (visited.find(current) == visited.end())
		{
			visitor(current);  // Visit this node
			visited.insert(current);  // Mark it as visited

			// Check all of its connections
			list<Edge> edges = g.GetEdges(current);
			for (auto edge : edges)
			{
				Node* considering = edge.node2;
				// If we haven't visited this node yet, add it to the queue
				if (visited.find(considering) == visited.end())
				{
					unvisited.push(considering);
				}
			}
		}

		// If the queue is empty, we're done.
		if (unvisited.size() == 0)
			break;

		// Get the next node from the queue
		current = unvisited.front();
		unvisited.pop();
	}
}

void PrintNodeID(Node* n)
{
	cout << n->id << endl;
}

int main(int argc, char argv[])
{
	ListGraph g;

	Node* A = new Node("A");
	Node* B = new Node("B");
	Node* C = new Node("C");
	Node* D = new Node("D");
	Node* E = new Node("E");

	g.AddBidirectionalEdge(A, B, 1.0f);
	g.AddBidirectionalEdge(A, C, 1.0f);
	g.AddBidirectionalEdge(C, D, 1.0f);
	g.AddBidirectionalEdge(B, D, 1.0f);
	// E is lonely...

	cout << "BFS from A" << endl;
	BreadthFirstSearch(g, A, PrintNodeID);

	cout << endl;

	cout << "BFS from E" << endl;
	BreadthFirstSearch(g, E, PrintNodeID);

	cout << endl;

	cout << "BFS from C" << endl;
	BreadthFirstSearch(g, C, PrintNodeID);

	cin.get();
	return 0;
}