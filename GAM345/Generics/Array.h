#pragma once


template<typename T>
class Array
{
public:
	int size;
	T* contents;

	Array(int initialSize);
	Array(const Array& other);
	~Array();

	Array& operator=(const Array& other);

	T& operator[](int index);
};



#include "Array.inl"