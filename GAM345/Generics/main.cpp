#include <iostream>
#include "Array.h"
using namespace std;

template<typename T>
T GetMax(T objA, T objB)
{
	return (objA > objB)? objA : objB;
}

class Currency
{
public:
	int dollars;
	int cents;

	Currency()
	{
		dollars = 0;
		cents = 0;
	}

	Currency(int dollars, int cents)
	{
		this->dollars = dollars;
		this->cents = cents;
	}

	bool operator>(const Currency& other)
	{
		return (dollars + cents / 100.0f) > (other.dollars + other.cents / 100.0f);
		// Alternatively: Compare dollars first, then cents
	}
};


int main(int argc, char* argv[])
{
	int a = 5;
	int b = 6;
	cout << GetMax(a, b) << endl; // Prints 6

	float c = 4.5f;
	float d = 7.8f;
	cout << GetMax(c, d) << endl;  // Prints 7.8?

	cout << GetMax<int>(a, d) << endl;  // Prints ???
	cout << GetMax<float>(a, d) << endl;  // Prints ???

	Currency e(5, 43);
	Currency f(3, 2);
	cout << GetMax(e, f).dollars << endl;  // Prints 5


	Array<int> myIntegers(10);

	Array<Currency> myCUrrencies(50);



	cin.get();
	return 0;
}