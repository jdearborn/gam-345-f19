
// Need an old-fashioned "include guard" for this file.
#ifndef __ARRAY_INL__
#define __ARRAY_INL__

#include "Array.h"


template<typename T>
Array<T>::Array(int initialSize)
{
	size = initialSize;
	contents = new T[size];
}

template<typename T>
Array<T>::Array(const Array<T>& other)
{
	size = other.size;
	contents = new T[size];
	memcpy(contents, other.contents, size * sizeof(T));
}


template<typename T>
Array<T>::~Array()
{
	delete[] contents;
}

template<typename T>
Array<T>& Array<T>::operator=(const Array<T>& other)
{
	delete[] contents;

	size = other.size;
	contents = new T[size];
	memcpy(contents, other.contents, size * sizeof(T));

	return *this;
}

template<typename T>
T& Array<T>::operator[](int index)
{
	return contents[index];
}


#endif
