#include <iostream>
#include <functional>
#include <string>
#include <limits>
#include <fstream>
#include <chrono>
#include "Vector.h"
using namespace std;
using namespace std::chrono;

void Swap(int& a, int& b)
{
	int temp = a;
	a = b;
	b = temp;
}

void BubbleSort(int numbers[], int N)
{
	// Moves things from the beginning to the end, depending on the values

	/*
	REPEAT N-1 times
		FOR each pair
			IF left > right
				Swap
			ENDIF
		ENDFOR
	ENDREPEAT
	*/

	for (int i = 0; i < N - 1; ++i)
	{
		bool breakEarly = true;
		for (int p = 0; p < N - 1 - i; ++p)
		{
			if (numbers[p] > numbers[p + 1])
			{
				Swap(numbers[p], numbers[p + 1]);
				breakEarly = false;
			}
		}
		if (breakEarly)
			break;
	}

}

void InsertionSort(int numbers[], int N)
{
	// March through the entire list once, 
	// finding the spot each element belongs to in the left (sorted) side.

	/*
	SET pointer to second item
	REPEAT until unsorted section is empty
		SELECT the first item in the unsorted section
		REPEAT until current item is larger than next sorted (left) item
			COMPARE current item with next sorted item
			MOVE sorted item to the right if it is bigger
		ENDREPEAT
		INSERT current item into this spot in sorted section
		ADVANCE the overall pointer
	ENDREPEAT
	*/

	for (int pointer = 1; pointer < N; ++pointer)
	{
		int current = numbers[pointer];
		for (int nextLeftSortedIndex = pointer - 1; nextLeftSortedIndex >= 0; --nextLeftSortedIndex)
		{
			if (current < numbers[nextLeftSortedIndex])
			{
				Swap(numbers[nextLeftSortedIndex], numbers[nextLeftSortedIndex + 1]);
			}
		}
	}
}

int FindMinimum(int* numbers, int count)
{
	//int minSoFar = numeric_limits<int>::max();
	if (count <= 0)
		return -1;

	int minIndexSoFar = 0;
	for (int i = 1; i < count; ++i)
	{
		if (numbers[minIndexSoFar] > numbers[i])
			minIndexSoFar = i;
	}

	return minIndexSoFar;
}

void SelectionSort(int numbers[], int count)
{
	/*
	REPEAT until unsorted section is empty
		FIND smallest (minimum) element in unsorted section
		SWAP with the current element
		ADVANCE to next element
	ENDREPEAT
	*/

	for (int i = 0; i < count - 2; i++)
	{
		int index = FindMinimum(&numbers[i + 1], count - i - 1);
		index += i + 1;

		Swap(numbers[index], numbers[i]);
	}
}


Vector<int> MergeVector(Vector<int> a, Vector<int> b)
{
	Vector<int> result;

	// Sort the leading elements into the result vector
	while (a.Size() > 0 && b.Size() > 0)
	{
		if (a[0] < b[0])
		{
			result.PushBack(a[0]);
			a.EraseAt(0);
		}
		else
		{
			result.PushBack(b[0]);
			b.EraseAt(0);
		}
	}

	// Put any remaining elements into the result
	for (int i = 0; i < a.Size(); ++i)
	{
		result.PushBack(a[i]);
	}
	for (int i = 0; i < b.Size(); ++i)
	{
		result.PushBack(b[i]);
	}

	return result;
}

Vector<int> MergeSortVector(const Vector<int>& v)
{
	if (v.Size() <= 1)
		return v;

	// Split further
	int sizeA = v.Size() / 2;
	int sizeB = v.Size() - sizeA;
	Vector<int> a = v.Subset(0, sizeA);
	Vector<int> b = v.Subset(sizeA, sizeB);

	// Recursively sort each subvector
	a = MergeSortVector(a);
	b = MergeSortVector(b);

	// Merging them together performs the actual sorting comparisons
	return MergeVector(a, b);
}

/*void MergeSort(int numbers[], int count)
{
	Vector<int> v(numbers, count);

	v = MergeSortVector(v);

	for (int i = 0; i < count; ++i)
	{
		numbers[i] = v[i];
	}
}*/


// mid is exclusive, end is exclusive
void Merge(int* numbers, int start, int mid, int end)
{
	int left = start;
	int right = mid;

	int* subarray = new int[end - start + 1];
	int i = 0;

	while (left < mid && right < end)
	{
		if (numbers[left] < numbers[right])
		{
			subarray[i] = numbers[left];
			++left;
		}
		else
		{
			subarray[i] = numbers[right];
			++right;
		}
		++i;
	}

	while (left < mid)
	{
		subarray[i] = numbers[left];
		++left;
		++i;
	}

	while (right < end)
	{
		subarray[i] = numbers[right];
		++right;
		++i;
	}


	memcpy(numbers + start, subarray, i * sizeof(int));
	delete[] subarray;
}

// end is exclusive
void MergeSortRecursive(int* numbers, int start, int end)
{
	if (end - start <= 1)
		return;

	int mid = (start + end) / 2;
	MergeSortRecursive(numbers, start, mid);
	MergeSortRecursive(numbers, mid, end);

	Merge(numbers, start, mid, end);
}

void MergeSort(int numbers[], int count)
{
	MergeSortRecursive(numbers, 0, count);
}




void QuickSort(int numbers[], int count)
{

}



void DoSort(std::function<void(int[], int)> SortAlgo, string name)
{
	int numbers[] = { 5, 2, 42, 55, 31, 42, 13, -1, 0, 3 };

	SortAlgo(numbers, 10);

	cout << name << ":" << endl;
	for (int i = 0; i < 10; ++i)
	{
		cout << numbers[i] << " ";
	}
	cout << endl;
}

void MeasureSort(std::function<void(int[], int)> SortAlgo, string name)
{
	ofstream fout;
	fout.open(name + "_data.txt");
	fout << "# " << name << " performance" << endl;
	fout << "# N\tNanoseconds" << endl;

	for (int n = 1; n < 10000; n += 100)
	{
		int* numbers = new int[n];
		for (int i = 0; i < n; ++i)
		{
			numbers[i] = rand();
		}

		high_resolution_clock::time_point start = high_resolution_clock::now();

		SortAlgo(numbers, n);

		high_resolution_clock::time_point finish = high_resolution_clock::now();
		nanoseconds totalNS = duration_cast<nanoseconds>(finish - start);

		fout << n << "\t" << totalNS.count() << endl;

		delete[] numbers;
	}
	fout.close();
}

int main(int argc, char* argv[])
{
	// BubbleSort, InsertionSort, SelectionSort

	/*MeasureSort(BubbleSort, "Bubble Sort");

	MeasureSort(InsertionSort, "Insertion Sort");

	MeasureSort(SelectionSort, "Selection Sort");

	MeasureSort(MergeSort, "Merge Sort");*/

	MeasureSort(QuickSort, "Quicksort");

	//DoSort(MergeSort, "Merge Sort");
	//cin.get();

	system("gnuplot plot-sorting.txt");

	return 0;
}