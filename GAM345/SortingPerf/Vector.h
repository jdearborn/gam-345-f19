#pragma once

template<typename T>
class Vector
{
public:
	
	// Rule of Three:
	// If you define any of: Destructor, Copy constructor, or Copy assignment operator...
	// Then you should define all three.
	Vector();
	Vector(const Vector& other);
	Vector(T* array, int count);
	~Vector();

	Vector& operator=(const Vector& other);


	void PushBack(const T& value);
	void PushFront(const T& value);
	void Insert(const T& value, int index);
	void Reserve(int newCapacity);  // Makes sure capacity is at least this value
	void Resize(int newSize);
	T& At(int index);
	T& operator[](int index);
	int Size() const;
	void Clear();
	void EraseAt(int index);
	void Erase(const T& value);
	int Find(const T& value);
	bool Contains(const T& value);
	Vector Subset(int start, int count) const;

private:
	int capacity;
	T* contents;
	int size;
};

#include "Vector.inl"