#include <iostream>
#include <functional>
#include "Vector.h"
using namespace std;

int AddUp(int* array, int count)
{
	int total = 0;
	for (int i = 0; i < count; ++i)
	{
		total += array[i];
	}

	return total;
}

int Fold(function<int(int,int)> operation, Vector<int> array)
{
	int total = 0;
	for (int i = 0; i < array.Size(); ++i)
	{
		total = operation(total, array[i]);
	}

	return total;
}

int Add(int a, int b)
{
	return a + b;
}

int Multiply(int a, int b)
{
	return a * b;
}





int MakeDouble(int value)
{
	return 2 * value;
}
// Map: Apply the given function (returns int, takes 1 int) to each element of a container, 
// returning a new container with these new values

Vector<int> Map(function<int(int)> fn, Vector<int> v)
{
	Vector<int> result;
	for (int i = 0; i < v.Size(); ++i)
	{
		result.PushBack(fn(v[i]));
	}
	return result;
}


bool IsGreaterThan5(int value)
{
	return value > 5;
}
// FindIf: Takes a function (returns bool, takes 1 int) and returns the index
// of the first element for which the function is true.


bool IsInRange12to20(int value)
{
	return (12 <= value && value <= 20);
}
// Filter: Returns a new container with only the values for which the given function
// evaluates to true.


int main(int argc, char argv[])
{
	function<int(int*, int)> fn;

	fn = AddUp;


	int array[] = {4, 5, 6};
	cout << "AddUp: " << fn(array, 3) << endl;

	Vector<int> vec;
	vec.PushBack(4);
	vec.PushBack(5);
	vec.PushBack(6);


	cout << "Fold(Add): " << Fold(Add, vec) << endl;
	cout << "Fold(Multiply): " << Fold(Multiply, vec) << endl;




	cout << "Fold(lambda): " << Fold([](int a, int b) {return a - b; }, vec) << endl;





	cin.get();
	return 0;
}