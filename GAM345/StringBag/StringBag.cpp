#include "StringBag.h"

/// CPP file defines the implementation



StringBag::StringBag()
{
	contents = new string[10];
	size = 0;
	capacity = 10;
}

StringBag::StringBag(int maximumSize)
{
	contents = new string[maximumSize];
	size = 0;
	capacity = maximumSize;
}

StringBag::~StringBag()
{
	delete[] contents;
}

void StringBag::Add(const string& newEntry)
{
	if (size < capacity)
	{
		contents[size] = newEntry;
		size++;
	}
}

int StringBag::Size() const
{
	return size;
}

bool StringBag::Contains(string target) const
{
	for (int i = 0; i < size; ++i)
	{
		if (contents[i] == target)
			return true;
	}
	return false;
}