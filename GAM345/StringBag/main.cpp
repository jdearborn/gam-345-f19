#include <iostream>
#include "StringBag.h"
#include <chrono>
using namespace std;
using namespace std::chrono;

double PerfTestStringBagContains(const StringBag& bag, int numOperations)
{
	high_resolution_clock::time_point start = high_resolution_clock::now();

	for (int i = 0; i < numOperations; ++i)
	{
		bag.Contains("Test string");
	}

	high_resolution_clock::time_point finish = high_resolution_clock::now();
	nanoseconds totalNS = duration_cast<nanoseconds>(finish - start);
	return totalNS.count() / (double)numOperations;
}

int main(int argc, char* argv[])
{
	StringBag bag1;

	StringBag* bag2 = new StringBag();

	StringBag bag3(25);

	string s = "Test string";
	bag1.Add(s);
	bag1.Add("Other thing");

	//cout << "Bag1 has it? " << bag1.Contains("Test string") << endl;
	//cout << "Bag2 has it? " << bag2->Contains("Test string") << endl;

	double result = PerfTestStringBagContains(bag1, 100000);
	cout << "Contains() using string in container (avg time): " << result << " ns" << endl;


	delete bag2;

	cin.get();

	return 0;
}