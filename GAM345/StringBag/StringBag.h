#pragma once
#include <string>
using namespace std;

/// Header file defines the interface
/*
StringBag() // Default constructor: Make room for 10 strings
StringBag(int) // Make room for the given number of strings
~StringBag()
Add(string) // Adds a single string to the container
Add(string[], int) // Adds several strings to the container from an array
Add(StringBag*) // Adds the contents of the given bag into this one
int Size() // Returns the number of strings in this bag
int GetCapacity()  // Returns the amount of allocated space
Remove(string value)  // Removes one string from the container: the first that matches
int CountOccurrences(string target)  // Returns the number of matching strings
bool Contains(string target)  // Returns true if this string is in the bag
Union(StringBag*)  // Adds only the strings that are not already in the bag

Implement the StringBag class and these functions in separate .h and .cpp files.
Important: You must provide a main.cpp that tests/demonstrates each of these functions.
*/

/*
Getters and Setters
(Accessors and Mutators)
*/

class StringBag
{
public:

	StringBag();
	StringBag(int maximumSize);
	~StringBag();

	void Add(const string& newEntry);
	int Size() const;

	bool Contains(string target) const;

private:
	string* contents;
	int size;
	int capacity;
};